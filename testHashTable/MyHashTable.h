#ifndef __MY_HASH_TABLE__
#define __MY_HASH_TABLE__
// =======================================================
#include<typeinfo>
#include<string>

using namespace std;

//
template<class KeyType, class ValueType>
struct MyElement{
	KeyType mKey;
	ValueType mValue;

	MyElement():mKey(),mValue(){}
	MyElement(KeyType in_key, ValueType in_value):mKey(in_key),mValue(in_value){}
	MyElement(const MyElement &a):mKey(a.mKey),mValue(a.mValue){}

	void operator = (const MyElement &a){
		mKey = a.mKey;
		mValue = a.mValue;
	}
};


template<class KeyType, class ValueType>
struct Node{
	Node *prev;
	Node *next;

	MyElement<KeyType, ValueType> mData;
	Node():prev(nullptr),next(nullptr),mData(){}
	Node(struct Node *in_prev, struct Node *in_next, MyElement<KeyType, ValueType> in_data):
		prev(in_prev),next(in_next),mData(in_data){}
	Node(struct Node *in_prev, struct Node *in_next, KeyType in_key, ValueType in_value):
		prev(in_prev),next(in_next),mData(in_key,in_value){}
};
template<class KeyType, class ValueType> 
using NodePtr = Node< class KeyType, class ValueType>*;



template<class KeyType, class ValueType>
class MyHashTable{
	const int INITIAL_SIZE;
	const int EXPAND_SIZE;
	int capacity;
	Node<KeyType, ValueType> *mBegin;


	unsigned long StringToUnsignedLong(string id){
		unsigned long result = 0;
		for(int i = 0; i != id.size(); i++){
			result = result*128 + id[i];
		}
		return result;
	}

	// several hash functions
	// going to design universal hash function

	/*unsigned int hash_mod_by_id(unsigned long id){
		unsigned int result = 0;
		result = id%67;
		return result;
	}
	unsigned int hash_multiply_by_id(unsigned long id){
		int count_n = 0;
		double n = (double)id;
		while(n>=1){
			n/=10;
			count_n++;
		}
		int count_m = 0;
		double m = id*id;
		while(m>=1){
			m/=10;
			count_m++;
		}
		int diff = count_r - count_n;
		diff = diff/2;
		int temp = 0;
		for(int i = diff+count_n; i != diff; i++){
			temp = 
		}
	}*/

	// return the bucket number
	unsigned int hash(KeyType in_key){
		type_info keyID = typeid(KeyType);
		unsigned int result = 0;
		// check several types, 
		// other types are similar��omit
		if(keyID == typeif(unsigned long)){
			result = in_key%capacity;
		}else if(keyID == typeid(string){
			result = StringToUnsignedLong(in_key)%capacity;
		}else{
			cerr<<"error : currently, so hash function match to this type.";
			throw("currently, so hash function match to this type.");
			return -1;
		}
	}
public:
	MyHashTable():INITIAL_SIZE(200),EXPAND_SIZE(100){
		mBegin = new Node<KeyType, ValueType>[INITIAL_SIZE];
		capacity = INITIAL_SIZE;
	}

	bool Insert(MyElement<KeyType, ValueType> &reference){
		int id = hash(reference.mKey);
		if(mBegin[id]){
			if(mBegin[id].next){
				NodePtr<KeyType,ValueType> p = mBegin[id].next;
				while(p->next){
					p = p->next;
				}
				p->next = new Node<KeyType,ValueType>;
				p->next->prev = p;
				p->next->mData = reference;
			}else{
				mBegin[id].next = new Node<KeyType,ValueType>;
				mBegin[id].next->prev = mBegin[id]
			}
		}else
			return false;
	}

	bool Search(KeyType in_key, ValueType &out_value){
		
	}
};













// =======================================================
#endif // !__MY_HASH_TABLE__
