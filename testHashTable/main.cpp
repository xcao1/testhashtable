// test for stl unordered map
// test for MyHashTable
#include<iostream>
#include<string>
#include<unordered_map>
using namespace std;

bool printElement(string key, unordered_map<string,string> map){
	unordered_map<string,string>::iterator iter = map.find(key);
	if(iter != map.end()){
		cout<<iter->second;
		return true;
	}else{
		cout<<"can not find";
		return false;
	}
}
int main(){
	unordered_map<string,string> a;
	a.insert(pair<string,string>("first","KKKK"));
	a.insert(pair<string,string>("second","HHHHH"));
	
	printElement("first",a);
	printElement("second",a);

	a.erase("first");
	printElement("first",a);
	printElement("second",a);
	
	system("pause");
	return 0;
}